var map;
var infowindow;
var directionsDisplay;
var directionsService;
var matrix;
var distance;
var streetViewService;
var panorama;
var markers = [];
var markerArray = [];
var travel_type;
var service;
var limits = 0;

// Liste des point d'intérêt 
var poi = [
	{name:'shopping_mall',libelle:'Supermarché',group:'Commerces et services',group_ico:'images/map_pins/commercesservices_ico.png',limit:10},
	{name:'grocery_or_supermarket',libelle:'Superette',group:'Commerces et services',group_ico:'images/map_pins/commercesservices_ico.png',limit:10},
	{name:'bakery',libelle:'Boulangerie',group:'Commerces et services',group_ico:'images/map_pins/commercesservices_ico.png',limit:10},
	{name:'post_office',libelle:'Poste',group:'Commerces et services',group_ico:'images/map_pins/commercesservices_ico.png',limit:10},
	{name:'school',libelle:'Ecole - Collège - Lycée',group:'Education et culture',group_ico:'images/map_pins/educationculture_ico.png',limit:10},
	{name:'university',libelle:'Université',group:'Education et culture',group_ico:'images/map_pins/educationculture_ico.png',limit:10},
	{name:'library',libelle:'Bibliothèque',group:'Education et culture',group_ico:'images/map_pins/educationculture_ico.png',limit:10},
	{name:'museum',libelle:'Musée',group:'Education et culture',group_ico:'images/map_pins/educationculture_ico.png',limit:10},
	{name:'subway_station',libelle:'Métro',group:'Transports',group_ico:'images/map_pins/transport_ico.png',limit:10},
	{name:'train_station',libelle:'Gare',group:'Transports',group_ico:'images/map_pins/transport_ico.png',limit:10},
	{name:'bus_station',libelle:'Bus',group:'Transports',group_ico:'images/map_pins/transport_ico.png',limit:10},
	{name:'taxi_stand',libelle:'Taxi',group:'Transports',group_ico:'images/map_pins/transport_ico.png',limit:10},
	{name:'restaurant',libelle:'Restaurant',group:'Autres',group_ico:'images/map_pins/autres_ico.png',limit:10},
	{name:'gym',libelle:'Complexe sportif',group:'Autres',group_ico:'images/map_pins/autres_ico.png',limit:10},
	{name:'movie_theater',libelle:'Théâtre - Cinéma',group:'Autres',group_ico:'images/map_pins/autres_ico.png',limit:10},
	{name:'hospital',libelle:'Hôpital',group:'Autres',group_ico:'images/map_pins/autres_ico.png',limit:10},
];

// Longitude - Latitude du programme
var place = {lat: 48.8934413, lng: 2.2629984};
// Informations du bureau de vente
var place_bv = {lat: 48.9034413, lng: 2.2729984};
var adresse_bv = "Boulevard Robespierre 78200 Poissy";
var horaires_bv = "Du lundi au vendredi de 18h30 à 20h";

// Style de la carte donné par Franck
var styles = [{"featureType":"water","stylers":[{"saturation":43},{"lightness":-11},{"hue":"#0088ff"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"hue":"#ff0000"},{"saturation":-100},{"lightness":99}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"color":"#808080"},{"lightness":54}]},{"featureType":"landscape.man_made","elementType":"geometry.fill","stylers":[{"color":"#ece2d9"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#ccdca1"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#767676"}]},{"featureType":"road","elementType":"labels.text.stroke","stylers":[{"color":"#ffffff"}]},{"featureType":"poi","stylers":[{"visibility":"off"}]},{"featureType":"landscape.natural","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#b8cb93"}]},{"featureType":"poi.park","stylers":[{"visibility":"on"}]},{"featureType":"poi.sports_complex","stylers":[{"visibility":"on"}]},{"featureType":"poi.medical","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","stylers":[{"visibility":"simplified"}]}];

// Création de la carte et recherche des poi à proximité
function initMap() {

	map = new google.maps.Map(document.getElementById('map'), {
		scrollwheel: false,
		center: place,
		zoom: 14
	});
	
	map.setOptions({styles: styles});
	
	infowindow = new google.maps.InfoWindow();
	directionsDisplay = new google.maps.DirectionsRenderer({ suppressMarkers: true });
	directionsService = new google.maps.DirectionsService;
	travel_type = google.maps.TravelMode.WALKING;
	matrix = new google.maps.DistanceMatrixService;
	service = new google.maps.places.PlacesService(map);
	streetViewService = new google.maps.StreetViewService();
	
	// Place le marker de la residence
	var marker_rp = new google.maps.Marker({
		map: map,
		position: place,
		icon: 'images/map_pins/programme.png',
		zIndex: google.maps.Marker.MAX_ZINDEX + 2
	});
	
	google.maps.event.addListener(marker_rp, 'click', function() { 
		infowindow.setContent("<b style='font-size:14px'>Découvrir l'environnement de votre futur résidence !</b><br/><br/><a style='cursor:pointer;' onclick='javascript: createStreetViewRP();'>Parcourir</a> avec <img src='images/ico_streetview.jpg' />");
		infowindow.open(map, marker_rp);
		map.setCenter(place);
	});
	
	document.getElementById('map_programme_ico').addEventListener('click', function() { 
		infowindow.setContent("<b style='font-size:14px'>Découvrir l'environnement de votre futur résidence !</b><br/><a style='cursor:pointer;' onclick='javascript: createStreetViewRP();'>Parcourir</a> avec <img src='images/ico_streetview.jpg' />");
		infowindow.open(map, marker_rp);
		map.setCenter(place);
	});
	
	// Place le marker du bureau de vente
	var marker_bv = new google.maps.Marker({
		map: map,
		position: place_bv,
		icon: 'images/map_pins/espacevente.png',
		zIndex: google.maps.Marker.MAX_ZINDEX + 1
	});
	
	var content_bv = "<b style='font-size:14px'>Bureau de vente</b><br/>"+adresse_bv+"<br/>";
	content_bv += "<a target='_blank' href='https://www.google.com/maps/dir//"+adresse_bv+"'>Comment m'y rendre</a>";
	content_bv += "<div style='margin:5px 0;color:#bababa;'>Horaires:<br/>"+horaires_bv+"</div>";
	streetViewService.getPanoramaByLocation(place_bv, 50, function (streetViewPanoramaData, status) {
		if (status === google.maps.StreetViewStatus.OK) {
			content_bv += "<hr style='margin:5px 0'/><a style='cursor:pointer;' onclick='javascript: createStreetViewBV();'>Parcourir</a> avec <img src='images/ico_streetview.jpg' />";
		}
	});
	
	google.maps.event.addListener(marker_bv, 'click', function() { 
		infowindow.setContent(content_bv);
		infowindow.open(map, marker_bv);
		map.setCenter(place_bv);
	});
	
	document.getElementById('map_espacevente_ico').addEventListener('click', function() {
		infowindow.setContent(content_bv);
		infowindow.open(map, marker_bv);
		map.setCenter(place_bv);
	});
}

// Création d'un marqueur par poi
function callback(results, status, type, limit) {
	if (status === google.maps.places.PlacesServiceStatus.OK) {
		for (var i = 0; i < results.length && i < limit; i++) {
			createMarker(results[i], type);
		}
		limits++;
		if (limits >= 6){
			var elems = document.getElementsByClassName("map_pin");
			for (e = 0; e < elems.length; e++) {
				if (hasClass(elems[e], "active")){
					elems[e].className = "map_pin active";
				}else{
					elems[e].className = "map_pin disabled";
				}
			}
		}
	}
}

// Création des marqueurs avec une infobulle personnalisée
function createMarker(loc, type) {
	var placeLoc = loc.geometry.location;
	var icon = loc.icon;
	
	if (!markers[type]){
		markers[type] = new Array();
	}
	
	var idmark = markers[type].length;
			
	var marker = markers[type][idmark] = new google.maps.Marker({
		map: map,
		position: placeLoc,
		icon: 'images/map_pins/'+type+'.png'
	});
	// console.log(loc);
	
	// service.getDetails({
		// placeId: loc.id
	// }, function(place, status) {
		// if (status === google.maps.places.PlacesServiceStatus.OK) {
			// console.log(place);
		// }
	// });

	
	
	var content = "<b style='font-size:14px'>"+loc.name+"</b><br/>"+loc.vicinity+"<br/>";
	content += "<a style='cursor:pointer;' data-traveltype='WALKING' class='map_trajet' onclick='javascript: clickToGoTo(this, \""+type+"\", "+idmark+");'>A pied</a><span class='map_distime distime_pied'></span> - <a style='cursor:pointer;' data-traveltype='DRIVING' class='map_trajet' onclick='javascript: clickToGoTo(this, \""+type+"\", "+idmark+");'>En voiture</a><span class='map_distime distime_voiture'></span>";
	streetViewService.getPanoramaByLocation(placeLoc, 50, function (streetViewPanoramaData, status) {
		if (status === google.maps.StreetViewStatus.OK) {
			content += "<hr style='margin:5px 0'/><a style='cursor:pointer;' onclick='javascript: createStreetView(this, \""+type+"\", "+idmark+");'>Parcourir</a> avec <img src='images/ico_streetview.jpg' />";
		}
	});
	
	google.maps.event.addListener(marker, 'click', function() { 
		infowindow.setContent(content);
		infowindow.open(map, marker);
		map.setCenter(placeLoc);
	});
	
	google.maps.event.addListener(infowindow,'closeclick',function(){
	   directionsDisplay.setMap(null);
	});
}

// Récupération de la distance entre les deux points
function callbackbis(results, status) {
	if (status == google.maps.DistanceMatrixStatus.OK) {
		return results.rows[0].elements[0].distance.text;
	}
}

// Ajoute sur la carte le tableau de marqueurs
function setMapOnAll(map, type) {
  for (var i = 0; i < markers[type].length; i++) {
	markers[type][i].setMap(map);
	markers[type][i].setVisible(true); 
  }
}

// Cache les marqueur du type demandé de la map
function clearMarkers(type) {
	setMapOnAll(null, type);
	limits--;
	if (limits < 6){
		var elems = document.getElementsByClassName("map_pin");
		for (e = 0; e < elems.length; e++) {
			if (hasClass(elems[e], "active")){
				elems[e].className = "map_pin active";
			}else{
				elems[e].className = "map_pin";
			}
		}
	}
	markers[type] = [];
}

// Affiche les marqueur du type demandé de la map
function showMarkers(type,limit) {
	service.nearbySearch({
		location: place,
		radius: 2000,
		types: [type]
	},  function(results, status){ callback(results, status, type, limit); });
}

// Création des légendes des poi
var groupe = "";
var html = "<div class='map_resbv'><div class='map_grpfixe'><div id='map_programme_ico' class='map_icon'><img class='img-responsive' src='images/map_pins/programme_ico.png'></div><div class='map_text'><p>Résidence</p><p class='subtext'>Découvrez l\'adresse exacte dans la brochure</p></div></div><div class='map_grpfixe'><div id='map_espacevente_ico' class='map_icon'><img class='img-responsive' src='images/map_pins/espacevente_ico.png'></div><div class='map_text'><p>Espace de vente</p><p class='subtext'>"+adresse_bv+"</p><p class='subtext'>"+horaires_bv+"</p></div></div></div><div  class='map_pois'>";
for (i = 0; i < poi.length; i++) { 
	if (poi[i].group != groupe){
		html += '<div class="map_group"><div class="map_group-header"><div class="map_icon"><img class="img-responsive" src="'+poi[i].group_ico+'"></div><div class="map_text">'+poi[i].group+'</div></div><div class="map_group-box">';
		groupe = poi[i].group;
	}
	html += '<div class="map_pin" data-type="'+poi[i].name+'" data-limit="'+poi[i].limit+'"><div class="map_img"><img class="img-responsive" src="images/map_pins/'+poi[i].name+'_ico.png"></div><div class="map_lib">'+poi[i].libelle+'</div></div>';
	if (!poi[(i + 1)] || poi[(i + 1)].group != groupe){
		html += '</div></div>';
	}
}
document.getElementById("pins").innerHTML = html;

function hasClass(element, cls) {
    return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
}

// Changement d'événement "onclick" au clic sur une légende de poi
var elems = document.getElementsByClassName("map_pin");
for (e = 0; e < elems.length; e++) {
	elems[e].addEventListener('click', function() {
		var type = this.getAttribute("data-type");
		var limit = parseInt(this.getAttribute("data-limit"));
		
		if (hasClass(this, "active")){
			this.className = "map_pin";
			clearMarkers(type);
		}else{
			if (limits < 6){
				this.className = "map_pin active";
				showMarkers(type,limit);
			}
		}
	});
}

// Modification du moyen de transport au clic sur un lien
function clickToGoTo(elem, type, idmark){
	var traveltype = elem.getAttribute("data-traveltype");
	var distimes = elem.parentElement.getElementsByClassName("map_distime");
	var pos = markers[type][idmark].position;
	
	for (var dt = 0; dt < distimes.length; dt++) {
		distimes[dt].innerHTML = "";
	}
	
	if (traveltype == "DRIVING"){
		travel_type = google.maps.TravelMode.DRIVING;
	}else{
		travel_type = google.maps.TravelMode.WALKING;
	}
	directionsDisplay.setMap(null);
	// infowindow.close();
	
	matrix.getDistanceMatrix({
		origins: [pos],
		destinations: [place],
		travelMode: travel_type,
		unitSystem: google.maps.UnitSystem.METRIC
	}, function(response, status) { 
		if (status == google.maps.DistanceMatrixStatus.OK) {
			if (traveltype == "DRIVING"){
				var distimes = elem.parentElement.getElementsByClassName("distime_voiture");
				for (var dt = 0; dt < distimes.length; dt++) {
					distimes[dt].innerHTML = " ("+response.rows[0].elements[0].distance.text+" - "+response.rows[0].elements[0].duration.text+")";
				}
			}else{
				var distimes = elem.parentElement.getElementsByClassName("distime_pied");
				for (var dt = 0; dt < distimes.length; dt++) {
					distimes[dt].innerHTML = " ("+response.rows[0].elements[0].distance.text+" - "+response.rows[0].elements[0].duration.text+")";
				}
			}
		}
	});
	
	calculateAndDisplayRoute(directionsDisplay, directionsService, place, pos, markerArray, map);
}

// Calcul et tracer de l'itinéraire
function calculateAndDisplayRoute(directionsDisplay, directionsService, place, destloc, markerArray, map) {
	for (var i = 0; i < markerArray.length; i++) {
		markerArray[i].setMap(null);
	}
	
	directionsDisplay.setMap(map);
	
	directionsService.route({
		origin: place,
		destination: destloc,
		travelMode: travel_type
	}, function(response, status) {
		if (status === google.maps.DirectionsStatus.OK) {
			directionsDisplay.setDirections(response);
		}
	});
}

// Affiche les groupes de poi au clic l'element parent
var groups = document.getElementsByClassName("map_group-header");
for (var i = 0; i < groups.length; i++) {
	groups[i].addEventListener("click", function(){
		if(this.parentElement.childNodes[1].style.display == "block"){
			this.parentElement.childNodes[1].style.display = 'none';
		}else{
			var grps = document.getElementsByClassName("map_group");
			for (var g = 0; g < grps.length; g++) {
				grps[g].childNodes[1].style.display = 'none';
			}
			this.parentElement.childNodes[1].style.display = 'block';
		}
	});
}

// Recentre la map
function goToCenterMap(){
	map.panTo(place);
}

function createStreetView(elem, type, idmark){
	var pos = markers[type][idmark].position;
	
	panorama = map.getStreetView();
	panorama.setPosition(pos);
	panorama.setPov(({
		heading: 265,
		pitch: 0
	}));
	panorama.setVisible(true);
}

function createStreetViewBV(){
	panorama = map.getStreetView();
	panorama.setPosition(place_bv);
	panorama.setPov(({
		heading: 265,
		pitch: 0
	}));
	panorama.setVisible(true);
}

function createStreetViewRP(){
	panorama = map.getStreetView();
	panorama.setPosition(place);
	panorama.setPov(({
		heading: 265,
		pitch: 0
	}));
	panorama.setVisible(true);
}